<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inventories extends MY_Controller 
{
	public function __construct() {
		parent::__construct(); 
		$this->check_auth(2);
	}

	public function index() 
	{
		$this->load->model('inventory_model');
		$this->load->library('form_validation');
		$this->data['title'] = 'Inventory';

		$this->data['inventories'] = $this->inventory_model->get_inventories();

		$this->render_page('inventories/index', $this->data);
	}

	public function add_stock() 
	{
		$this->load->model('inventory_model');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('count', 'Count', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->data['title'] = 'Inventory';

			$this->data['inventories'] = $this->inventory_model->get_inventories();

			$this->render_page('inventories/index', $this->data);
		} else {
			$this->inventory_model->add_stock();

			$this->session->set_flashdata('inventory_stock_added', 'Inventory stock count has been updated (Add)');
			redirect('inventories/index');
		}
	}

	public function add_new_item() 
	{
		$this->load->model('inventory_model');
		$this->load->model('manufacturer_model');
		$this->load->model('category_model');
		$this->load->library('form_validation');

		$this->data['title'] = "Add New Inventory Item";

		$this->data['manufacturers'] = $this->manufacturer_model->get_manufacturers();
		$this->data['categories'] = $this->category_model->get_categories();

		$this->form_validation->set_rules('manufacturer_id', 'Manufacturer', 'required');
		$this->form_validation->set_rules('category_id', 'Category', 'required');
		$this->form_validation->set_rules('name', 'Product Name', 'required');
		$this->form_validation->set_rules('description', 'Product Description', 'required');
		$this->form_validation->set_rules('count', 'Count', 'required|numeric');

		if ($this->form_validation->run() === FALSE) {
			$this->render_page('inventories/add_new_item', $this->data);
		} else {
			$this->inventory_model->add_new_inventory();
			$this->session->set_flashdata('new_inventory_added', 'New Inventory Item has been Added');
			redirect('inventories/index');
		}

		
	}
}

?>