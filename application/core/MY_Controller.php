<?php

class MY_Controller extends CI_Controller {
	protected $data;

	public function __construct() {
		parent::__construct();
		
	}

	public function check_auth($page_type = 0){ // 0=login; 1=admin; 2=inventory; 3=sale;
		if ($page_type == 0){
			if ($this->session->userdata('user_logged_in')) {
				redirect('pages/initial_page');
			}
		}

		if ($page_type > 0){
			if (!$this->session->userdata('user_logged_in')) {
				redirect('users/login');
			}

			if ($this->session->userdata('usertype_id') != $page_type) {
				redirect('pages/initial_page');
			}
		}
	}

	public function render_page($page) {
		$this->load->view('templates/header', $this->data);
		$this->load->view($page, $this->data);
		$this->load->view('templates/footer');
	}
}

?>