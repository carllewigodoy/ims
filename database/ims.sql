-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2017 at 04:40 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ims`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Office Supplies', 'Supplies used in offices', 1, '2017-04-29 15:54:00', NULL),
(2, 'Carpentry Supplies', 'Supplies used in school', 1, '2017-04-29 15:54:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(255) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `serial_no`, `manufacturer_id`, `category_id`, `name`, `description`, `count`, `created_at`, `updated_at`) VALUES
(1, '100001', 1, 2, 'Golden Hammer', 'Hammer made out of pure Gold', 3, '2017-04-29 16:25:34', '2017-05-01 15:49:02'),
(2, '100002', 2, 1, 'Ballpen', 'Writing material with ink', 1, '2017-04-29 16:44:05', '2017-05-01 15:48:45'),
(4, '100003', 2, 2, 'Concrete Nails 7 In.', '7 Inches Concrete Nail used to nail your momma', 500, '2017-05-01 22:21:07', NULL),
(5, '100004', 1, 2, 'Sample Product', 'Sample Description', 25, '2017-05-01 23:28:18', NULL),
(6, '100006', 1, 2, 'Pradak', 'Pradak Description', 800, '2017-05-01 23:40:45', NULL),
(7, '100007', 1, 2, 'Seventh Product', 'Ika-Pitong Produkto', 7, '2017-05-01 23:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_stock_histories`
--

CREATE TABLE IF NOT EXISTS `inventory_stock_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `count_change` int(11) NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `inventory_stock_histories`
--

INSERT INTO `inventory_stock_histories` (`id`, `inventory_id`, `action`, `count_change`, `updated_by`, `created_at`) VALUES
(1, 1, 'Added Stock Count', 2, '9', '2017-05-01 21:48:24'),
(2, 2, 'Added Stock Count', 1, '9', '2017-05-01 21:48:45'),
(3, 1, 'Added Stock Count', 1, '9', '2017-05-01 21:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Manufacturer 1', 'Description of Manufacturer 1', 1, '2017-04-29 15:50:40', NULL),
(2, 'Manufacturer 2', 'Description of Manufacturer 2', 1, '2017-04-29 15:50:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventories_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `inventories_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, '1000.00', '2017-04-30 18:04:44', '2017-05-01 17:29:49'),
(2, 2, '2000.00', '2017-04-30 18:04:44', '2017-05-01 17:29:49'),
(4, 4, '30.00', '2017-05-01 23:15:23', '2017-05-01 17:29:49'),
(5, 5, '5.00', '2017-05-01 23:28:18', '2017-05-01 17:37:10'),
(6, 6, '0.00', '2017-05-01 23:40:45', NULL),
(8, 7, '7.00', '2017-05-01 23:41:33', '2017-05-01 17:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `price_histories`
--

CREATE TABLE IF NOT EXISTS `price_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventories_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `price_histories`
--

INSERT INTO `price_histories` (`id`, `inventories_id`, `price`, `created_at`) VALUES
(1, 5, '3.00', '2017-05-01 23:36:57'),
(2, 5, '5.00', '2017-05-01 23:37:10'),
(3, 7, '0.00', '2017-05-01 23:41:33'),
(4, 7, '70.00', '2017-05-01 23:42:09'),
(5, 7, '7.00', '2017-05-01 23:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE IF NOT EXISTS `sales_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `inventories_id` int(11) NOT NULL COMMENT 'product_id',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `usertype_id` int(11) NOT NULL COMMENT '1=admin; 2=inventory; 3=sales',
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `usertype_id`, `registered_at`) VALUES
(3, 'sales123', '0ad80eb119d9bf7775aa23786b05b391', 'sales123@gmail.com', 3, '2017-04-21 21:12:10'),
(5, 'sales505', '42ff484684f9ab1a3360330847ffddcb', 'sales505@gmail.com', 3, '2017-04-22 00:28:50'),
(6, 'admin456', '1a145a23d6e47aadfe2063f1f951e691', 'admin456@gmail.com', 1, '2017-04-22 03:32:19'),
(9, 'inventory99', '8dfa21905fa68abf8527deb6593fa105', 'Inventory99@gmail.com', 2, '2017-04-22 03:35:51'),
(11, 'admin123', '0192023a7bbd73250516f069df18b500', 'admin123@gmail.com', 1, '2017-04-22 05:17:49'),
(12, 'inventory123', '5d1086fbcd28c81a419e12317432251a', 'inventory123@gmail.com', 2, '2017-04-29 18:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE IF NOT EXISTS `usertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Inventory'),
(3, 'Sales');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
