<div class="text-center"><?php echo validation_errors(); ?></div>
<div class="row">
	<div class="col-md-6 col-md-offset-1" style="background-color:;">
		<h2 class="text-center"><?= $title; ?></h2>
		<?php echo form_open('admins/register'); ?>
			<div class="form-group">
				<label>User Type</label>
				<select class="form-control" name="usertype_id">
					<?php foreach($usertypes as $usertype): ?>
						<?php if(isset($user_input['usertype']) && $user_input['usertype'] == $usertype['id']): ?>
							<option value="<?php echo $usertype['id']; ?>" selected><?php echo $usertype['name']; ?></option>
						<?php else: ?>
							<option value="<?php echo $usertype['id']; ?>"><?php echo $usertype['name']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control" value="<?php if(isset($user_input['username'])) echo $user_input['username']; ?>">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control">
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="password" name="conf_password" class="form-control">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" class="form-control" value="<?php if(isset($user_input['email'])) echo $user_input['email']; ?>">
			</div>
			<button type="submit" class="btn btn-primary btn-block">Submit</button>
		<?php echo form_close(); ?>
	</div>

	<div class="col-md-3 col-md-offset-1" style="background-color:;">
		<h3 class="text-center">Generate Accounts</h3>
		<?php echo form_open('admins/generate_accounts'); ?>
			<div class="form-group">
				<label>Number of accounts to generate</label>
				<input type="number" name="count" class="form-control" min=1 value=1>
			</div>
			<button type="submit" class="btn btn-primary btn-block">Generate</button>
		<?php echo form_close(); ?>
	</div>

</div>



<?php echo form_open('admins/generate_accounts'); ?>

<?php echo form_close(); ?>