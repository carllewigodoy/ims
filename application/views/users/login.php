<?php echo form_open('users/login'); ?>
	<h2 class="text-center"><?= $title; ?></h2>
	<div class="text-center"><?php echo validation_errors(); ?></div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control" value="<?php if(isset($user_input['username'])) echo $user_input['username']; ?>">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary btn-block">Submit</button>
		</div>
	</div>
<?php echo form_close(); ?>