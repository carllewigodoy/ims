<?php 

class Price_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function update_price(){
		$inventories_id = $this->input->post('id');

		
		$data_history = array(
			'inventories_id' => $inventories_id,
			'price' => $this->input->post('price')
		);

		$data = array(
			'price' => $this->input->post('price'),
			'updated_at' => date('Y-m-d H:i:s')
		);


		$this->db->insert('price_histories', $data_history); // insert history
		
		return $this->db->update('prices', $data, array('inventories_id' => $inventories_id)); // update
	}
}

?>