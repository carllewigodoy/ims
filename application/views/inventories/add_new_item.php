<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<?php echo form_open('inventories/add_new_item'); ?>
		<h2 class="text-center"><?= $title; ?></h2>
		<div class="text-center"><?php echo validation_errors(); ?></div>
		<div class="form-group">
			<label>Serial No</label>
			<input type="text" class="form-control" name="serial_no" placeholder="Serial No">
		</div>
		<div class="form-group">
			<label>Manufacturer</label>
			<select class="form-control" name="manufacturer_id">
				<?php foreach($manufacturers as $manufacturer): ?>
					<option value="<?php echo $manufacturer['id']; ?>"><?php echo $manufacturer['name']; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Category</label>
			<select class="form-control" name="category_id">
				<?php foreach($categories as $category): ?>
					<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label>Product Name</label>
			<input type="text" class="form-control" name="name" placeholder="Product Name">
		</div>
		<div class="form-group">
			<label>Product Description</label>
			<input type="text" class="form-control" name="description" placeholder="Product Description">
		</div>
		<div class="form-group">
			<label>Count</label>
			<input type="number" class="form-control" name="count" min=0 value="1">
		</div>
		<button type="submit" class="btn btn-primary btn-block">Submit</button>
		<?php echo form_close(); ?>
	</div>
</div>