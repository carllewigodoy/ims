<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct(); 
		$this->check_auth(3);
	}

	public function index() 
	{
		$this->data['title'] = 'Sales Page';

		$this->render_page('sales/index', $this->data);
	}
}

?>