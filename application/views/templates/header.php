<html>
	<head>
		<title>IMS - <?= $title; ?></title>

		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Bootstrap CSS theme -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flatly/bootstrap.min.css">

		<!-- Datatables -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
		
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo base_url();?>home">IMS</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="<?php if($title=='Home') echo 'active' ;?>"><a href="<?php echo base_url();?>home">Home</a></li>
						<li class="<?php if($title=='About') echo 'active' ;?>"><a href="<?php echo base_url();?>about">About</a></li>
						<?php if($this->session->userdata('user_logged_in') && $this->session->userdata('usertype_id') == 1): ?>
							<li class="<?php if($title=='Accounts') echo 'active' ;?>"><a href="<?php echo base_url();?>admins/index">Accounts</a></li>
							<li class="<?php if($title=='Inventory Prices') echo 'active' ;?>"><a href="<?php echo base_url();?>admins/prices">Prices</a></li>
						<?php endif; ?>

						<?php if($this->session->userdata('user_logged_in') && $this->session->userdata('usertype_id') == 2): ?>
							<li class="<?php if($title=='Inventory') echo 'active' ;?>"><a href="<?php echo base_url();?>inventories/index">Inventory</a></li>
						<?php endif; ?>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						
						<?php if(!$this->session->userdata('user_logged_in')): ?>
							<li class="<?php if($title=='Login') echo 'active' ;?>"><a href="<?php echo base_url();?>users/login">Login</a></li>
						<?php endif; ?>

						<?php if($this->session->userdata('user_logged_in') && $this->session->userdata('usertype_id') == 1): ?>
							<li class="<?php if($title=='Create Account') echo 'active' ;?>"><a href="<?php echo base_url();?>admins/register">Create Account</a></li>
						<?php endif; ?>

						<?php if($this->session->userdata('user_logged_in') && $this->session->userdata('usertype_id') == 2): ?>
							<li class="<?php if($title=='Add New Inventory Item') echo 'active' ;?>"><a href="<?php echo base_url();?>inventories/add_new_item">Add Inventory</a></li>
						<?php endif; ?>
						
						<?php if($this->session->userdata('user_logged_in')): ?>
							<li class="<?php if($title=='Logout') echo 'active' ;?>"><a href="<?php echo base_url();?>users/logout">Logout</a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</nav>
		
		<div class="container">

		<!-- Display flashdata -->
		<?php if($this->session->flashdata('user_registered')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('user_registered'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('users_updated')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('users_updated'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('login_success')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('login_success'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('login_failed')): ?>
			<p class="alert alert-danger"><?php echo $this->session->flashdata('login_failed'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('inventory_stock_added')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('inventory_stock_added'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('new_inventory_added')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('new_inventory_added'); ?></p>
		<?php endif; ?>

		<?php if($this->session->flashdata('price_updated')): ?>
			<p class="alert alert-success"><?php echo $this->session->flashdata('price_updated'); ?></p>
		<?php endif; ?>


		