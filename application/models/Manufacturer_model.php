<?php

class Manufacturer_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_manufacturers(){
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('manufacturers');

		return $query->result_array();
	}
}

?>