<h2><?= $title; ?></h2>

<!-- <pre><?php print_r($inventories); ?> </pre> -->

<div class="text-center"><?php echo validation_errors(); ?></div>
<table id="inventory_table">
	<thead>
		<tr>
			<td>Serial No</td>
			<td>Manufacturer</td>
			<td>Category</td>
			<td>Product</td>
			<td>Description</td>
			<td>Price</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($inventories as $inventory): ?>
			<tr>
				<td><?php echo $inventory['serial_no']; ?></td>
				<td><?php echo $inventory['manufacturer_name']; ?></td>
				<td><?php echo $inventory['category_name']; ?></td>
				<td><?php echo $inventory['name']; ?></td>
				<td><?php echo $inventory['description']; ?></td>
				<td class="text-right"><?php echo $inventory['price']; ?></td>
				<td><button type="button" id="<?php echo $inventory['id']; ?>" class="btn btn-default btn-edit-price" data-toggle="modal" data-target="#edit-price-modal">Update Price</button></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<!-- Edit price modal-->
<div id="edit-price-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Update Price</h4>
		</div>

		<div class="modal-body">
			<?php echo form_open('admins/prices'); ?>
				<input id="inventory-id" type="hidden" name="id" class="form-control">
				<div class="form-group">
					<label>Price</label>
					<input id="edit-price" type="text" name="price" class="form-control">
				</div>
				<button type="submit" class="btn btn-primary btn-block">Submit</button>
			<?php echo form_close(); ?>
		</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

	</div>
</div>

<script>
	$('#inventory_table').DataTable();

	$('.btn-edit-price').click(function(){
		var id = $(this).attr('id');
		$('#inventory-id').val(id);

		price = $(this).closest('td').prev('td').text();
		$('#edit-price').val(price);
	});
</script>