<?php

class Admin_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_usertypes(){
		$query = $this->db->get('usertypes');
		return $query->result_array();
	}

	public function check_username_availability($username){
		$query = $this->db->get_where('users', array('username' => $username));
		if(empty($query->row_array())){
			return true;
		} else {
			return false;
		}
	}

	public function check_email_availability($email){
		$query = $this->db->get_where('users', array('email' => $email));
		if(empty($query->row_array())){
			return true;
		} else {
			return false;
		}
	}

	public function check_username_availability_edit($username, $id){
		//$query = $this->db->get_where('users', array('username' => $username));
		$this->db->where('id != ', $id);
		$this->db->where('username', $username);

		$query = $this->db->get('users');

		if($query->num_rows() > 0 ){
			return $query->row(0)->id;
		}
	}

	public function check_email_availability_edit($email, $id){
		//$query = $this->db->get_where('users', array('username' => $username));
		$this->db->where('id != ', $id);
		$this->db->where('email', $email);

		$query = $this->db->get('users');

		if($query->num_rows() > 0 ){
			return $query->row(0)->id;
		}
	}
}
?>