<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends MY_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->check_auth(1);
	}

	public function index() 
	{
		$this->load->model('admin_model');
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->data['title'] = 'Accounts';
		$this->data['users'] = $this->user_model->get_users();
		$this->data['usertypes'] = $this->admin_model->get_usertypes();

		$this->render_page('admins/index', $this->data);	
	}

	public function register() 
	{
		$this->load->model('admin_model');
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->data['title'] = "Create Account";

		$this->form_validation->set_rules('usertype_id', 'User Type', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[6]|callback_check_username_availability');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('conf_password', 'Confirm Password', 'matches[password]');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_availability');

		$this->data['usertypes'] = $this->admin_model->get_usertypes();

		if ($this->form_validation->run() === FALSE) {
			$this->data['user_input'] = $this->input->post();

			$this->render_page('admins/register', $this->data);
		} else {
			$password = $this->input->post('password');
			$enc_pwd = $this->encrypt_password($password);

			$_data = array(
				'username' => $this->input->post('username'),
				'password' => $enc_pwd,
				'email' => $this->input->post('email'),
				'usertype_id' => $this->input->post('usertype_id'),
			);

			$this->user_model->create($_data);

			$this->session->set_flashdata('user_registered', 'User registration successful');
			redirect('admins/index');
		}
	}

	public function generate_accounts()
	{
		$this->load->model('admin_model');
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->data['title'] = "Create Account";

		$this->form_validation->set_rules('count', 'Number of Accounts', 'required');

		if ($this->form_validation->run() === FALSE ){
			$this->render_page('admins/register', $this->data);
		} else {
			require_once 'vendor/autoload.php';

			$faker = Faker\Factory::create('en_PH');

			$count = $this->input->post('count');
			$count = 2;
			for ($i=0; $i <= $count; $i++) {
				$_data = array();
				$_data['username'] = $faker->unique->userName;
				$_data['password'] = $faker->md5;
				$_data['email'] = $faker->unique->email;
				$_data['usertype_id'] = $faker->biasedNumberBetween($min = 1, $max = 3);

				$this->user_model->create($_data);
			}

			$this->session->set_flashdata('user_registered', 'User generation successful');
			redirect('admins/index');

		}
	}

	public function edit_user() 
	{
		$this->load->model('admin_model');
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('usertype_id', 'User Type', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[6]|callback_check_username_availability_edit');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('conf_password', 'Confirm Password', 'matches[password]');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_availability_edit');

		if ($this->form_validation->run() === FALSE) {
			$this->data['title'] = 'Accounts';
			$this->data['users'] = $this->user_model->get_users();
			$this->data['usertypes'] = $this->admin_model->get_usertypes();

			$this->render_page('admins/index', $this->data);
		} else {
			$password = $this->input->post('password');
			$enc_pwd = $this->encrypt_password($password);

			$this->user_model->update($enc_pwd);
			$this->session->set_flashdata('user_updated', 'User has been updated');
			redirect('admins/index');
		}
	}

	public function delete_user() 
	{
		$this->load->model('user_model');

		$this->user_model->delete_user();
		$this->session->set_flashdata('user_deleted', "User has been deleted");
		redirect('admins/index');
	}

	public function prices() 
	{
		$this->load->model('price_model');
		$this->load->model('inventory_model');
		$this->load->library('form_validation');

		$this->data['title'] = 'Inventory Prices';

		$this->data['inventories'] = $this->inventory_model->get_inventories();

		$this->form_validation->set_rules('price', 'Price', 'required|numeric');

		if ($this->form_validation->run() === FALSE) {
			$this->render_page('admins/prices', $this->data);
		} else {
			$this->price_model->update_price();
			$this->session->set_flashdata('price_updated', 'Price has been updated');
			redirect('admins/prices');
		}
	}

	// functions
	public function encrypt_password($password) 
	{
		return md5($password);
	}

	public function check_username_availability($username) 
	{
		$this->load->model('admin_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('check_username_availability', 'The username <strong>"'.$username.'"</strong> was already taken. Choose a different one');
		if ($this->admin_model->check_username_availability($username)) {
			return true;
		} else {
			return false;
		}
	}

	public function check_email_availability($email) 
	{
		$this->load->model('admin_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('check_email_availability', 'The email <strong>"'.$email.'"</strong> was already taken. Choose a different one');
		if ($this->admin_model->check_email_availability($email)) {
			return true;
		} else {
			return false;
		}
	}

	public function check_auth2() 
	{
		if(!$this->session->userdata('user_logged_in')) {
			redirect('users/login');
		}

		if($this->session->userdata('usertype_id') != 1) {
			redirect('users/initial_page');
		}
	}

	public function check_username_availability_edit($username) 
	{
		$this->load->model('admin_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('check_username_availability_edit', 'The username <strong>"'.$username.'"</strong> was already taken. Choose a different one');
		$id = $this->input->post('id');
		if ($result = $this->admin_model->check_username_availability_edit($username, $id)) {
			return false;
		} else {
			return true;
		}
	}

	public function check_email_availability_edit($email) 
	{
		$this->load->model('admin_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('check_email_availability_edit', 'The email <strong>"'.$email.'"</strong> was already taken. Choose a different one');
		$id = $this->input->post('id');
		if ($result = $this->admin_model->check_email_availability_edit($email, $id)) {
			return false;
		} else {
			return true;
		}
	}
}

?>