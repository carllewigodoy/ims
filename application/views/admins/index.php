<h2><?= $title; ?></h2>

<p>This is admin index page.</p>

<div class="text-center"><?php echo validation_errors(); ?></div>

<table class="display" id="user_table">
	<thead>
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Usertype</th>
			<th>Registered at</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($users as $user): ?>
			<tr id="<?php echo $user['id']; ?>">
				<td class="td-username"><?php echo $user['username']; ?></td>
				<td class="td-email"><?php echo $user['email']; ?></td>
				<td class="td-usertype_id"><?php echo $user['usertype_id']." - ".$user['usertype_name']; ?></td>
				<td class="td-registered_at"><?php echo $user['registered_at']; ?></td>
				<td>
					<button id="<?php echo $user['id']; ?>" type="button" class="btn btn-warning btn-edit" data-toggle="modal" data-target="#edit-modal">Edit</button> <!-- href="users/edit/<?php echo $user['id']; ?>" -->
					<button id="<?php echo $user['id']; ?>" type="button" class="btn btn-danger btn-delete" data-toggle="modal" data-target="#delete-modal">Delete</button> <!-- href="users/edit/<?php echo $user['id']; ?>" -->
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<!-- Edit Modal -->
<div id="edit-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit</h4>
		</div>

		<div class="modal-body">
			<?php echo form_open('admins/edit_user'); ?>
				<input id="edit-id" type="hidden" name="id" class="form-control">
				<div class="form-group">
					<label>Username</label>
					<input id="edit-username" type="text" name="username" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input id="edit-email" type="email" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label>Password</label>
					<input id="edit-password" type="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<label>Confirm Password</label>
					<input id="edit_confpassword" type="password" name="conf_password" class="form-control">
				</div>
				<div class="form-group">
					<label>Usertype</label>
					<select id="edit-usertype_id" name="usertype_id" class="form-control">
						<?php foreach($usertypes as $usertype): ?>
							<option value="<?php echo $usertype['id']; ?>"><?php echo $usertype['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Submit</button>
			<?php echo form_close(); ?>
		</div>
		<div class="modal-footer">

			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

	</div>
</div>


<!-- Delete Modal -->
<!-- Edit Modal -->
<div id="delete-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit</h4>
		</div>

		<div class="modal-body">
			<?php echo form_open('admins/delete_user'); ?>
				<input id="delete-id" type="hidden" name="id" class="form-control">
				<p>Are you sure you want to delete this row?</p>
				<button type="submit" class="btn btn-primary btn-block">Confirm</button>
			<?php echo form_close(); ?>
		</div>
		<div class="modal-footer">

			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

	</div>
</div>


<script>
	$(document).ready(function() {
	    $('#user_table').DataTable({
	        "order": [[ 0, "asc" ]]
	    });

	    $('.btn-edit').click(function(){
	    	var id = $(this).attr('id'); // user_id
	    	var username = $(this).closest('tr').children('td.td-username').text(); // username
	    	var email = $(this).closest('tr').children('td.td-email').text(); // email
	    	var usertype = $(this).closest('tr').children('td.td-usertype_id').text(); // usertype (1 - Admin)
	    	var usertype_id = usertype.split(" -")[0]; // usertype_id (1)

	    	$('#edit-id').val(id);
	    	$('#edit-username').val(username);
	    	$('#edit-email').val(email);
	    	$('#edit-usertype_id').val(usertype_id);
	    });

	    $('.btn-delete').click(function(){
	    	var id = $(this).attr('id'); // user_id
	    	$('#delete-id').val(id);

	    });
	} );
</script>

