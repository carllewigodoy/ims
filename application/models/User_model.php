<?php

class User_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function create($data){
		

		return $this->db->insert('users', $data);
	}

	public function update($enc_pwd){
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $enc_pwd,
			'email' => $this->input->post('email'),
			'usertype_id' => $this->input->post('usertype_id'),
		);

		$this->db->where('id', $this->input->post('id'));
		return $this->db->update('users', $data);
	}

	public function delete_user(){
		$this->db->where('id', $this->input->post('id'))->delete('users');
		return true;
	}

	public function check_login($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);

		$query = $this->db->get('users');

		if($query->num_rows() == 1 ){
			return $query->row(0);
		}
	}

	public function get_users(){
		$this->db->select('users.*, usertypes.name as usertype_name');
		$this->db->join('usertypes', 'users.usertype_id = usertypes.id');
		$this->db->order_by('users.id', 'DESC');
		$query = $this->db->get('users');
		return $query->result_array();
	}
}

?>