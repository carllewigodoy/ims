<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends My_Controller 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function login() 
	{
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->check_auth(0);

		$this->data['title'] = "Login";

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->render_page('users/login', $this->data);
		} else {
			$username = $this->input->post('username');
			$enc_pwd = $this->encrypt_password($this->input->post('password'));

			if ($user = $this->user_model->check_login($username, $enc_pwd)) {
				$user_id = $user->id;
				$usertype_id = $user->usertype_id;

				$userdata = array (
					'user_id' => $user_id,
					'username' => $username,
					'user_logged_in' => true,
					'usertype_id' => $usertype_id
				);

				$this->session->set_userdata($userdata);

				$this->session->set_flashdata('login_success', 'You are now logged in');
				redirect('users/login');
			} else {
				$this->session->set_flashdata('login_failed', 'Login Invalid');
				$this->render_page('users/login', $this->data);
			}
		}
	}

	public function logout() 
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('user_logged_in');
		$this->session->unset_userdata('usertype_id');

		redirect('users/login');
	}

	public function encrypt_password($password) 
	{
		return md5($password);
	}

	public function encrypt_password2($password) 
	{
		$this->load->library('encrypt');
		return $this->encrypt->encode($password);
	}
}

?>