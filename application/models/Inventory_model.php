<?php 

class Inventory_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_inventories(){
		$this->db->select('inventories.*, manufacturers.name as manufacturer_name, categories.name as category_name, prices.price');
		$this->db->join('manufacturers', 'inventories.manufacturer_id = manufacturers.id');
		$this->db->join('categories', 'inventories.category_id = categories.id');
		$this->db->join('prices', 'prices.inventories_id = inventories.id', 'left');
		//$this->db->join('prices as p1', 'p1.id = (select max(id) from prices as p2 where p2.inventories_id = p1.inventories_id)', 'left');
		$this->db->order_by('serial_no');
		$query = $this->db->get('inventories');

		return $query->result_array();
	}

	public function add_stock(){
		$query1 = $this->db->get_where('inventories', array('id'=>$this->input->post('id')));
		$original_count = $query1->row_array()['count'];

		$data_history = array(
			'inventory_id' => $this->input->post('id'),
			'action' => 'Added Stock Count',
			'count_change' => $this->input->post('count'),
			'updated_by' => $this->session->userdata('user_id')
		);

		$this->db->insert('inventory_stock_histories', $data_history);

		$data = array(
			'count' => $original_count + $this->input->post('count'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->update('inventories', $data, array('id'=>$this->input->post('id')));
	}

	public function add_new_inventory(){
		$data = array(
			'serial_no' => $this->input->post('serial_no'),
			'manufacturer_id' => $this->input->post('manufacturer_id'),
			'category_id' => $this->input->post('category_id'),
			'name' => $this->input->post('name'),
			'description' => $this->input->post('description'),
			'count' => $this->input->post('count')
		);

		$this->db->insert('inventories', $data);

		$insert_id = $this->db->insert_id();
		$data_prices = array(
			'inventories_id' => $insert_id,
			'price' => 0.00
		);

		$this->db->insert('price_histories', $data_prices); // insert to price history table
		return $this->db->insert('prices', $data_prices); // insert initial price
	}
}

?>