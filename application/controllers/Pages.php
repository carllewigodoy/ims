<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller 
{
	public function index() 
	{
		$this->render_page('pages/home', $this->data);
	}

	public function home() 
	{
		$this->data['title'] = "Home";
		$this->data['content'] = "Inventory Management System Home page";

		$this->render_page('pages/home', $this->data);
	}

	public function about() 
	{
		$this->data['title'] = "About";
		$this->data['content'] = "Inventory Management System About page";

		$this->render_page('pages/about', $this->data);
	}

	public function initial_page() 
	{
		if ($this->session->userdata('usertype_id') == 1) {
			redirect('admins/index');
		} 
		if ($this->session->userdata('usertype_id') == 2) {
			redirect('inventories/index');
		}
		if ($this->session->userdata('usertype_id') == 3) {
			redirect('sales/index');
		}
	}
}

?>