<h2><?= $title; ?></h2>

<!-- <pre><?php print_r($inventories); ?> </pre> -->

<div class="text-center"><?php echo validation_errors(); ?></div>
<table id="inventory_table">
	<thead>
		<tr>
			<td>Serial No</td>
			<td>Manufacturer</td>
			<td>Category</td>
			<td>Product</td>
			<td>Description</td>
			<td>Count</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach($inventories as $inventory): ?>
			<tr>
				<td><?php echo $inventory['serial_no']; ?></td>
				<td><?php echo $inventory['manufacturer_name']; ?></td>
				<td><?php echo $inventory['category_name']; ?></td>
				<td><?php echo $inventory['name']; ?></td>
				<td><?php echo $inventory['description']; ?></td>
				<td class="text-center"><?php echo $inventory['count']; ?></td>
				<td><button type="button" id="<?php echo $inventory['id']; ?>" class="btn btn-default btn-add-stock" data-toggle="modal" data-target="#add-stock-modal">Add Stock</button></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<!-- Add stock modal-->
<div id="add-stock-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Add Stock</h4>
		</div>

		<div class="modal-body">
			<?php echo form_open('inventories/add_stock'); ?>
				<input id="inventory-id" type="hidden" name="id" class="form-control">
				<div class="form-group">
					<label>How many?</label>
					<input id="add-count" type="number" name="count" class="form-control" min=1 value="1">
				</div>
				<button type="submit" class="btn btn-primary btn-block">Submit</button>
			<?php echo form_close(); ?>
		</div>

		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>

	</div>
</div>

<script>
	$('#inventory_table').DataTable();

	$('.btn-add-stock').click(function(){
		var id = $(this).attr('id');
		$('#inventory-id').val(id);
	});
</script>